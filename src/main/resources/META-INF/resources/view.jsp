<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<%@ include file="/init.jsp" %>

<h1>Pruebas con fechas</h1>

<%
	Date d = new Date();
Locale es = new Locale("es","ES");
Locale eus = new Locale("eu");
Locale en = new Locale("en", "GB");
Locale gl = new Locale("gl", "ES");
	Locale[] loc = {es, eus, en, gl};
	
%>

<p>Locales disponibles: <%=Arrays.toString(Locale.getAvailableLocales())%></p>

<%
for(Locale l:loc){
	%>
	<h3>locale es <%=l.toString() %></h3>
	<%
	DateFormat df1 = DateFormat.getDateInstance(DateFormat.SHORT, l);
	%>
	<p><%=df1.format(d) %></p>
	<%
	DateFormat df2 = DateFormat.getDateInstance(DateFormat.MEDIUM, l);
	%>
	<p><%=df2.format(d) %></p>
	<%
	DateFormat df3 = DateFormat.getDateInstance(DateFormat.LONG, l);
	%>
	<p><%=df3.format(d) %></p>
	<%
	DateFormat df4 = DateFormat.getDateInstance(DateFormat.FULL, l);
	%>
	<p><%=df4.format(d) %></p>
<%
}
%>
