# Fechas

Pruebas de internalización de fechas para locales no soportados por la JVM mediante SPIs. Usando los proyectos:

+ [Java Galician](https://www.javagalician.org/): añade traducción de fechas y formatos para los locales *"gl"* y *"gl_ES"*.
+ [Java-basque-locale](https://github.com/emiperez/java-basque-locale/blob/master/java-basque-locale/src/main/java/com/kirolak/spi/eu/LocaleServiceProvider_eu.java): añade traducción de fechas (y sus correspondientes formatos) para el local *"eu"*.

**Nota:** Para usarlos, desplegar en la carpeta */lib/ext* del **jre** usado para la ejecución de Liferay los jars correspondientes a cada proyecto, siguiendo las especificaciones de [los Mecanismos de Extensiones](https://docs.oracle.com/javase/8/docs/technotes/guides/extensions/index.html).

**Alternativas:** Ejecutar la JVM usando el repositorio [CLDR de unicode](http://cldr.unicode.org/) (a partir de JAVA 8, [adoptado por defecto en JAVA 9+](https://docs.oracle.com/javase/9/intl/internationalization-enhancements-jdk-9.htm#JSINT-GUID-5ED91AA9-B2E3-4E05-8E99-6A009D2B36AF), [con posibilidad de uso en JAVA 8](https://docs.oracle.com/javase/8/docs/technotes/guides/intl/enhancements.8.html)) para proveer los formatos y traducciones.